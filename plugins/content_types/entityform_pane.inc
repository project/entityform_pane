<?php

/**
 * @file
 * Plugin to handle the embedding of the contents of a entityform into a panel.
 */

/**
 * Plugins are described by creating a $plugin array.
 *
 * This will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Entityform pane'),
  'single' => TRUE,
  'defaults' => array(
    'entityform_bundle' => '',
    'links' => TRUE,
    'leave_node_title' => FALSE,
    'identifier' => '',
    'build_mode' => 'teaser',
  ),
  'icon' => 'icon_content.png',
  'description' => t('Add an entityform as a pane.'),
  'category' => t('Custom'),
  'top level' => TRUE,
  'edit form' => 'entityform_pane_edit_form',
  'render callback' => 'entityform_pane_render',
  'admin info' => 'entityform_pane_admin_info',
);

/**
 * Output function for the entityform_pane content type.
 */
function entityform_pane_render($subtype, $conf, $panel_args) {
  module_load_include('inc', 'entityform', 'entityform.admin');
  $bundle = ($conf['entityform_bundle']);
  // Load an empty entityform with the same bundle as the config_form.
  $entityform = entityform_empty_load($bundle);
  // Build the entityform.
  $build = entityform_form_wrapper($entityform, 'submit', 'embedded');
  $build['#view_mode'] = 'default';

  $block = new stdClass();
  $block->content = $build;
  $block->title = isset($build['#entity']) ? $build['#entity']->title : '';
  return $block;
}

/**
 * The form to add or edit a entityform bundle as content.
 */
function entityform_pane_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  if ($form_state['op'] == 'add') {
    $form['entityform_bundle'] = array(
      '#prefix' => '<div class="no-float">',
      '#title' => t('Enter the entityform bundle machine name'),
      '#type' => 'textfield',
      '#maxlength' => 512,
      '#weight' => -10,
      '#suffix' => '</div>',
    );
  }
  else {
    $form['entityform_bundle'] = array(
      '#type' => 'value',
      '#value' => $conf['entityform_bundle'],
    );
  }

  return $form;
}

/**
 * Validate the entityform selection.
 */
function entityform_pane_edit_form_submit($form, &$form_state) {
  $form_state['conf']['entityform_bundle'] = $form_state['values']['entityform_bundle'];
}

/**
 * Returns the administrative title for a entityform.
 */
function entityform_pane_admin_title($subtype, $conf) {
  return $conf['entityform_bundle'];
}

/**
 * Display the administrative information for a entityform_pane pane.
 */
function entityform_pane_admin_info($subtype, $conf) {
  // Just render the entityform machine name.
  $block = new stdClass();
  $block->title = $conf['entityform_bundle'];
  return $block;
}
